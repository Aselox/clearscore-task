package util.view

import android.animation.ArgbEvaluator
import android.content.Context
import android.support.v4.content.ContextCompat
import com.borisdamato.clearscoretask.R

fun ArgbEvaluator.calculateScoreColor(context: Context, score: Int, max: Int): Int {
    return evaluate(
            score.toFloat() / max,
            ContextCompat.getColor(context, R.color.md_orange_a200),
            ContextCompat.getColor(context, R.color.md_light_green_a200)
    ) as Int
}