package util.rx

import io.reactivex.Scheduler

/**
 * Abstraction interface for RxJava's Schedulers. Useful for unit testing purposes.
 */
interface SchedulerProvider {

    fun ui(): Scheduler

    fun io(): Scheduler
}