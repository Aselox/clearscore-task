package com.borisdamato.clearscoretask

import android.app.Activity
import android.app.Application
import com.borisdamato.clearscoretask.di.AppAutoInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class ClearScoreTaskApp: Application(), HasActivityInjector {
    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    override fun onCreate() {
        super.onCreate()

        AppAutoInjector.init(this)
    }
}
