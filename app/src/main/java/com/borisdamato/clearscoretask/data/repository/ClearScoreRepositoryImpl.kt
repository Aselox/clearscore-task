package com.borisdamato.clearscoretask.data.repository

import com.borisdamato.clearscoretask.data.api.ClearScoreAPI
import com.borisdamato.clearscoretask.data.model.ScoreResponse
import io.reactivex.Single

class ClearScoreRepositoryImpl(private val api: ClearScoreAPI) : ClearScoreRepository {

    override fun getCreditScore(): Single<ScoreResponse> = api.getScore()
}