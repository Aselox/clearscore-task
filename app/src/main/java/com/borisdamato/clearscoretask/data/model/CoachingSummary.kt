package com.borisdamato.clearscoretask.data.model

data class CoachingSummary(
        val activeTodo: Boolean, 
        val activeChat: Boolean, 
        val numberOfTodoItems: Int, 
        val numberOfCompletedTodoItems: Int, 
        val selected: Boolean
)