package com.borisdamato.clearscoretask.data.model

data class ScoreResponse(
        val accountIDVStatus: String,
        val creditReportInfo: CreditReportInfo, 
        val dashboardStatus: String, 
        val personaType: String, 
        val coachingSummary: CoachingSummary
)