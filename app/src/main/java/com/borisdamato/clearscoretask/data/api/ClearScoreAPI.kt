package com.borisdamato.clearscoretask.data.api

import com.borisdamato.clearscoretask.data.model.ScoreResponse
import io.reactivex.Single
import retrofit2.http.GET

interface ClearScoreAPI {

    /**
     * Requests the report about the credit score of the current user
     * @return [Single] containing a [ScoreResponse] object]
     */
    @GET("mockcredit/values")
    fun getScore(): Single<ScoreResponse>
}