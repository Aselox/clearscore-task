package com.borisdamato.clearscoretask.data.repository

import com.borisdamato.clearscoretask.data.model.ScoreResponse
import io.reactivex.Single

interface ClearScoreRepository {

    fun getCreditScore(): Single<ScoreResponse>
}