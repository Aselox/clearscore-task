package com.borisdamato.clearscoretask.ui.dashboard

import android.arch.lifecycle.MutableLiveData
import com.borisdamato.clearscoretask.base.BaseViewModel
import com.borisdamato.clearscoretask.data.model.Resource
import com.borisdamato.clearscoretask.data.model.ScoreResponse
import com.borisdamato.clearscoretask.data.repository.ClearScoreRepository
import util.rx.SchedulerProvider
import javax.inject.Inject

/**
 * This class is responsible of fetching and providing data for [DashboardActivity]
 * @param repository The repository that calls the API asking for data
 * @param schedulerProvider Implementation of [SchedulerProvider] interface. Provides abstraction layer
 * useful for testing purposes.
 *
 * @constructor When this ViewModel is instantiated, it immediately fetches data from the repository
 * and exposes it to [DashboardActivity] via the [creditScore] property using [MutableLiveData]
 */
class DashboardViewModel @Inject constructor(
        private val repository: ClearScoreRepository,
        private val schedulerProvider: SchedulerProvider) : BaseViewModel() {

    val creditScore = MutableLiveData<Resource<ScoreResponse>>()

    var shouldAnimate = true

    init {
        fetchCreditScore()
    }

    private fun fetchCreditScore() {
        repository.getCreditScore()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    disposables.add(it)
                    creditScore.value = Resource.Loading()
                }
                .subscribe({
                    creditScore.value = Resource.Success(it)
                }, {
                    creditScore.value = Resource.Error(it.message.toString())
                })
    }

    /**
     * Re-fetches the [ClearScoreRepository] asking for fresh data and
     * resets the [shouldAnimate] property to true
     */
    fun refresh() {
        shouldAnimate = true
        fetchCreditScore()
    }
}