@file:Suppress("UNCHECKED_CAST")

package com.borisdamato.clearscoretask.ui.dashboard

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.borisdamato.clearscoretask.data.api.ClearScoreAPI
import com.borisdamato.clearscoretask.data.repository.ClearScoreRepository
import com.borisdamato.clearscoretask.data.repository.ClearScoreRepositoryImpl
import dagger.Module
import dagger.Provides
import util.rx.SchedulerProvider

/**
 * This class is a Dagger module and is responsible of providing the dependencies needed to
 * instantiate [DashboardViewModel]
 */
@Module
class DashboardActivityModule {

    @Provides
    fun provideClearScoreRepository(api: ClearScoreAPI): ClearScoreRepository =
            ClearScoreRepositoryImpl(api)

    @Provides
    fun provideDashboardViewModelFactory(
            repository: ClearScoreRepository,
            schedulerProvider: SchedulerProvider
    ): ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T =
                    DashboardViewModel(repository, schedulerProvider) as T
        }
    }
}