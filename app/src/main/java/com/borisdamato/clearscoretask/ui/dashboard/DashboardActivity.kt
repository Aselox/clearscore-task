package com.borisdamato.clearscoretask.ui.dashboard

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.animation.DecelerateInterpolator
import com.borisdamato.clearscoretask.R
import com.borisdamato.clearscoretask.data.model.Resource
import kotlinx.android.synthetic.main.activity_dashboard.*
import util.view.calculateScoreColor
import javax.inject.Inject

/**
 * Activity that displays a donut view representing the credit score which is being passed
 * by the [DashboardViewModel]
 */
class DashboardActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DashboardViewModel::class.java)

        viewModel.creditScore.observe(this, Observer {
            it?.let {
                when (it) {
                    is Resource.Loading -> {
                        swipeRefreshLayout.isRefreshing = true
                    }
                    is Resource.Success -> {
                        swipeRefreshLayout.isRefreshing = false

                        val creditReport = it.data.creditReportInfo

                        creditScoreMax.text = getString(R.string.credit_score_max_param, creditReport.maxScoreValue)
                        showScore(creditReport.score, creditReport.maxScoreValue, viewModel.shouldAnimate)
                    }
                    is Resource.Error -> {
                        swipeRefreshLayout.isRefreshing = false
                        Snackbar.make(coordinatorLayout, it.message, Snackbar.LENGTH_SHORT).show()
                    }
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener(this)
    }

    override fun onRefresh() {
        viewModel.refresh()
    }

    /**
     * Shows the user's credit score and, if requested, handles animations for the ProgressBar
     * progress value and the creditScore TextView text and textColor
     *
     * @param value User's credit score
     * @param maxValue Max possible value for the user's credit score
     * @param animate Whether it should animate the creditScore value from 0 to [value]
     */
    private fun showScore(value: Int, maxValue: Int, animate: Boolean) {
        scoreProgress.max = maxValue

        val colorEvaluator = ArgbEvaluator()

        if (!animate) {
            scoreProgress.progress = value
            creditScore.text = value.toString()

            colorEvaluator.calculateScoreColor(this, value, maxValue).also {
                creditScore.setTextColor(it)
            }

            return
        }

        ValueAnimator.ofInt(0, value).apply {
            duration = 1000
            interpolator = DecelerateInterpolator().apply { }
            addUpdateListener {
                val animatedScore = it.animatedValue as Int

                scoreProgress.progress = animatedScore
                creditScore.text = animatedScore.toString()

                colorEvaluator.calculateScoreColor(this@DashboardActivity, animatedScore, maxValue).also {
                    creditScore.setTextColor(it)
                }

                addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator?) {
                        viewModel.shouldAnimate = false
                    }
                })
            }
        }.start()
    }
}
