package com.borisdamato.clearscoretask.di

import com.borisdamato.clearscoretask.ui.dashboard.DashboardActivity
import com.borisdamato.clearscoretask.ui.dashboard.DashboardActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [DashboardActivityModule::class])
    abstract fun bindDashboardActivity(): DashboardActivity
}