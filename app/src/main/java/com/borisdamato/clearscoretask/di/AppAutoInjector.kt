package com.borisdamato.clearscoretask.di

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.borisdamato.clearscoretask.ClearScoreTaskApp
import dagger.android.AndroidInjection

object AppAutoInjector {

    fun init(app: ClearScoreTaskApp) {
        DaggerAppComponent.builder().application(app).build().inject(app)

        app.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                AndroidInjection.inject(activity)
            }

            override fun onActivityStarted(activity: Activity?) {

            }

            override fun onActivityResumed(activity: Activity?) {

            }

            override fun onActivityPaused(activity: Activity?) {

            }

            override fun onActivityStopped(activity: Activity?) {

            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

            }

            override fun onActivityDestroyed(activity: Activity?) {

            }

        })
    }
}