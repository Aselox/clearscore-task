package com.borisdamato.clearscoretask.di

import dagger.Module
import dagger.Provides
import util.rx.AppSchedulerProvider
import util.rx.SchedulerProvider
import javax.inject.Singleton

@Module
class AppModule {

    @Singleton
    @Provides
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()
}