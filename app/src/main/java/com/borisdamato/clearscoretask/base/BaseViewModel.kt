package com.borisdamato.clearscoretask.base

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * Base class extending [ViewModel] that includes all common code between app's viewModels
 */
open class BaseViewModel : ViewModel() {

    protected val disposables = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
    }
}