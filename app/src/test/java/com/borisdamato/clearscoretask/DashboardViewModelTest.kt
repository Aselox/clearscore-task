package com.borisdamato.clearscoretask

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.borisdamato.clearscoretask.data.model.Resource
import com.borisdamato.clearscoretask.data.model.ScoreResponse
import com.borisdamato.clearscoretask.data.repository.ClearScoreRepository
import com.borisdamato.clearscoretask.ui.dashboard.DashboardViewModel
import com.borisdamato.clearscoretask.util.TestSchedulerProvider
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class DashboardViewModelTest {

    @Mock
    private lateinit var repository: ClearScoreRepository

    @Mock
    private lateinit var response: ScoreResponse

    @Mock
    private lateinit var livedataObserver: Observer<Resource<ScoreResponse>>

    private lateinit var viewModel: DashboardViewModel

    private val schedulerProvider = TestSchedulerProvider()

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `when created, should fetch repository`() {
        successfulResponse()

        createViewModel()

        verify(repository).getCreditScore()
    }

    @Test
    fun `when fetches repository successfully, should set success livedata`() {
        successfulResponse()

        createViewModel()
        viewModel.creditScore.observeForever(livedataObserver)

        assert((viewModel.creditScore.value as? Resource.Success)?.data == response)
        verify(livedataObserver).onChanged(viewModel.creditScore.value)
    }

    @Test
    fun `when fetches repository unsuccessfully, should set error livedata`() {
        val error = Throwable("some error")
        `when`(repository.getCreditScore()).thenReturn(Single.error(error))

        createViewModel()
        viewModel.creditScore.observeForever(livedataObserver)

        assert((viewModel.creditScore.value as? Resource.Error)?.message == error.message)
        verify(livedataObserver).onChanged(viewModel.creditScore.value)
    }

    @Test
    fun `when asked to refresh, should re-fetch repository`() {
        successfulResponse()

        createViewModel()
        viewModel.refresh()

        verify(repository, times(2)).getCreditScore()
    }

    private fun successfulResponse() {
        `when`(repository.getCreditScore()).thenReturn(
                Single.just(response)
        )
    }

    private fun createViewModel() {
        viewModel = DashboardViewModel(repository, schedulerProvider)
    }
}