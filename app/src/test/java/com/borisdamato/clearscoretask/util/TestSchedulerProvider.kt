package com.borisdamato.clearscoretask.util

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import util.rx.SchedulerProvider

class TestSchedulerProvider : SchedulerProvider {
    override fun ui(): Scheduler = Schedulers.trampoline()

    override fun io(): Scheduler = Schedulers.trampoline()
}